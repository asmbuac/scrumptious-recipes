from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm, StepForm
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory

# Create your views here.
@login_required
def create_recipe(request):
    StepFormSet = formset_factory(StepForm, extra=3, min_num=2, validate_min=True)
    if request.method == 'POST':
        form = RecipeForm(request.POST)
        formset = StepFormSet(request.POST)
        if all([form.is_valid(), formset.is_valid()]):
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            for inline_form in formset:
                if inline_form.cleaned_data:
                    steps = inline_form.save(False)
                    steps.recipe = recipe
                    steps.save()
            return redirect('recipe_list')
    else:
        # Create an instance of RecipeForm
        form = RecipeForm()
        formset = StepFormSet()

    context = {
        'form': form,
        'formset': formset,
    }

    return render(request, 'recipes/create.html', context)

@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect('show_recipe', id=id)
    else:
        form = RecipeForm(instance=recipe)

    context = {
        'form': form,
        'recipe_object': recipe,
    }

    return render(request, 'recipes/edit.html', context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        'recipe_list': recipes,
    }
    return render(request, 'recipes/list.html', context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        'recipe_list': recipes,
    }
    return render(request, 'recipes/list.html', context)

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        'recipe_object': recipe,
    }
    return render(request, 'recipes/detail.html', context)
