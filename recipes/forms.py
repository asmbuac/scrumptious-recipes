from django.forms import ModelForm
from recipes.models import Recipe, RecipeStep

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            'title',
            'picture',
            'description'
        ]

class StepForm(ModelForm):
    class Meta:
        model = RecipeStep
        fields = [
            'step_number',
            'instruction'
        ]
