# Generated by Django 4.1.5 on 2023-01-12 19:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_rename_text_recipe_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='picture',
            field=models.URLField(blank=True),
        ),
    ]
