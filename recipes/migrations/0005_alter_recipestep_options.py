# Generated by Django 4.1.5 on 2023-01-17 19:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_recipestep'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipestep',
            options={'ordering': ['order']},
        ),
    ]
